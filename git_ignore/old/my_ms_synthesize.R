my_ms_synthesize = function (script, token = NULL, api_key = NULL, gender = c("Female",
                                                                           "Male"), language = "en-US", voice = NULL, output_format = c("raw-16khz-16bit-mono-pcm",
                                                                                                                                        "raw-8khz-8bit-mono-mulaw", "riff-8khz-8bit-mono-alaw", "riff-8khz-8bit-mono-mulaw",
                                                                                                                                        "riff-16khz-16bit-mono-pcm", "audio-16khz-128kbitrate-mono-mp3",
                                                                                                                                        "audio-16khz-64kbitrate-mono-mp3", "audio-16khz-32kbitrate-mono-mp3",
                                                                                                                                        "raw-24khz-16bit-mono-pcm", "riff-24khz-16bit-mono-pcm",
                                                                                                                                        "audio-24khz-160kbitrate-mono-mp3", "audio-24khz-96kbitrate-mono-mp3",
                                                                                                                                        "audio-24khz-48kbitrate-mono-mp3"), escape = FALSE, region = NULL,
                          api = c("tts", "bing"), ...)
{
  region = ms_region(region)
  if (!is.null(voice)) {
    L = ms_voice_info(voice, token = token, api_key = api_key,
                      region = region)
  }
  else {
    L = ms_validate_language_gender(language = language,
                                    gender = gender)
  }

  language = L$language
  gender = L$gender
  xname = L$full_name[1]
  synth_url = ms_synthesize_api_url(api = api, region = region)
  if (is.null(token)) {
    token = ms_get_tts_token(api_key = api_key, region = region)$token
    print(token)

  }

  auth_hdr = add_headers(Authorization = token)
  output_format = match.arg(output_format)
  fmt_hdr = add_headers(`X-Microsoft-OutputFormat` = output_format)

  ctype = content_type("application/ssml+xml")
  ssml = ms_create_ssml(script = script, gender = gender, language = language,
                        voice = voice, escape = escape)

  if (nchar(ssml) > 1024) {
    cat(ssml)
    stop("Need smaller script! SSML is over 1024 characters")
  }
  res = POST(synth_url, body = ssml, auth_hdr, fmt_hdr, ctype,
             auth_hdr, ...)

  print(res)
  stop_for_status(res)
  print("4")
  out = content(res)
  print("5")
  class(token) = "token"
  print("6")
  L = list(request = res, ssml = ssml, content = out, token = token,
           output_format = output_format)
  return(L)
}
