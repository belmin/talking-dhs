---
title: "generate a DHS story"
author: "Camille Belmin"
date: "1/14/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(here)
library(tidyverse)
```

```{r}

month_num_to_name = data.frame(month_num = 1:12,
                               month_name = c("January", "February", "March", "April", "May",
                                              "June", "July", "August", "September", "October",
                                              "November", "December"))

path_dhs_data = paste0(str_remove(here(), "talking_DHS"), "DHS_data/") # the DHS data is in the parent directory because I use DHS data in several projects
path_IR_data = paste0(path_dhs_data, "IR/") # path to individual recode

metadata_dhs_IR <- read.csv(here("data","metadata_dhs_IR.csv"))

```

```{r}
# Functions used in this code

prepare_dhs = function(dhs_dat){

  dhs_dat %>%
    select(caseid, v000, # country code and phase
           age = v012,
           month_num = v006, year_interview = v007,
           year_birth = v010, region= v024, educ = v107,
           #  v115: time to get to watersource
           wall_material = v128,
           roof_material = v129, religion = v130, ethnicity = v131,
         #  watch_tv_freq = v159,
           son_died =  v206, dau_died = v207,
           pregn_term = v228) %>%
    mutate(country_code_2 = substr(v000,start = 1, stop = 2)) %>%
    left_join(metadata_dhs_IR %>% select(country_code_2, country_name, country_code),
              by = "country_code_2") %>%
    left_join(month_num_to_name, by = "month_num") %>%
    left_join(stack(attr(.$religion, 'labels')) %>%
                rename(religion = values, religion_name = ind ),
              by = "religion") %>%
    left_join(stack(attr(.$roof_material, 'labels')) %>%
                rename(roof_material = values, roof_material_name = ind ),
              by = "roof_material") %>%
    left_join(stack(attr(.$wall_material, 'labels')) %>%
                rename(wall_material = values, wall_material_name = ind ),
              by = "wall_material")

}

# This function generates a story from DHS variables in a DHS survey. 
# Now it always uses the same variables, but ultimately it could pick different variables each time

generate_story = function(dat){

  str1 = paste0("My name is ", str_remove_all(dat$caseid, " "),
               ", we are in ",
               dat$month_name, " ",dat$year_interview,
               " and I live in ", dat$country_name,".",
               " I am ", dat$age, " year old and I identify as a ",
               dat$religion_name, ".",
               " The roof of my household is made mainly out of ", dat$roof_material_name,
               " and the walls are made out of ", dat$wall_material_name, "."
               )
  if(dat$pregn_term == 1){
    str = paste0(str1, " I have had ", dat$pregn_term," miscarriage.")
  } else if (dat$pregn_term > 1){
     str = paste0(str1, " I have had ", dat$pregn_term," miscarriages.")
  }  else(
    str = paste0(str1," I had never had a miscarriage." )
  )

  return(str)

}

```

# Test one DHS survey

```{r }
i = runif(1,1, nrow(metadata_dhs_IR))
dhs_dat_full =  readRDS(file = paste0(path_dhs_data,"IR/",
                                      metadata_dhs_IR$file_name[i])) %>% as.data.frame()# data for the current country, one year

dhs_dat= prepare_dhs(dhs_dat_full)

one_respondent = dhs_dat[sample(nrow(dhs_dat), 1), ]
story = generate_story(one_respondent)
       
# Generate a small text for image generation
paste(one_respondent$caseid,
      one_respondent$month_name,
      one_respondent$year_interview,
      one_respondent$country_name, 
      one_respondent$age, "year old",
      one_respondent$religion_name,
      one_respondent$roof_material_name, "roof", 
      one_respondent$wall_material_name, "walls" )

   
```


# 1. Generate 10 stories out of DHS random DHS surveys 

```{r }

# Make a look on 10 survey and generate the story 

# While loop 
dhs_stories = tibble(story = "", audio_file_name ="", .rows = 11)
n = 0
while (n < 11) {
    # pick a random number 
  r = round(runif(1,1, nrow(metadata_dhs_IR)))
  dhs_dat_full = 
    readRDS(file = paste0(path_dhs_data,"IR/",
                          metadata_dhs_IR$file_name[r])) %>% as.data.frame()
    tryCatch(
     expr = { 
       dhs_dat= prepare_dhs(dhs_dat_full)
       one_respondent = dhs_dat[sample(nrow(dhs_dat), 1), ]
       story = generate_story(one_respondent)
        # iterator of number of times it worked
        n = n+1
        dhs_stories$audio_file_name[n] = paste0("story_", metadata_dhs_IR$SurveyId[r],"_",r ,".wav")
        dhs_stories$story[n] = story
        print(n)

       },
    error = function(err){
      print("go to the next survey")
     # r = round(runif(1,1, nrow(metadata_dhs_IR)))
      }
    
  )
  
}

dhs_stories$story[6]
write.csv(dhs_stories,here("data", "derived_data", "stories_set3.csv"))



#paste(dat$caseid, dat$month_name, dat$year_interview, dat$country_name, dat$age, "year old", dat$religion_name,dat$roof_material_name,
 #     "roof", dat$wall_material_name, "walls" )


# relevant variables:
# caseid:
#   v000: country code and phase
# v006: month of interview
# v007: year of interview
# v010: repondent year of birth
# v024/v025: region
# v107: highest year of education
# v115: time to get to watersource
# v128: main wall material
# v129: main roof material
# v130: religion
# v131: ethnicity
# v159: frequency of watching tv
# v206: sons who have died
# v207: daughters who have died
# v228: ever had a terminated preg



```

# 2. Choose 10 surveys, each in a different country and generate 10 stories per survey

# 3. Choose 10 countries, and generate 10 stories per country (the stories for one country can come from different surveys)

```{r}
# countries = metadata_dhs_IR$country_code %>% unique()

```
