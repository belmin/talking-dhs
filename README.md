# Talking DHS data


Talking DHS data is a web application that allows users to hear short stories about
thousands of respondents of Demographic and Health Survey from the global South and their living
conditions. The stories are generated from the real data of these respondents, and
they narrated in the first person using a text-to-speech engines. 

The web app of the protype version of Talking-DHS data can be found [here](https://talking-dhs.shinyapps.io/shiny_app/). The app was developed with R shiny and the code will be shared in the near future. 

This repository contains the code to generate text and audio stories from Demographic and Health Survey respondents. 

[code/generate_stories.Rmd](https://gitlab.pik-potsdam.de/belmin/talking-dhs/-/tree/master/code/generate_stories.Rmd) allows to generate stories as text, using R. Generating the stories requires the user to have DHS data installed on their computer. The stories as text are as a csv at: data/derived_data/stories_setX.csv 

[code/generate_audios.ipynb](https://gitlab.pik-potsdam.de/belmin/talking-dhs/-/tree/master/code/generate_audios.ipynb) allows to generate the audio from the stories. It uses Microsoft Azure text-to-speech service. The user who wants to reproduce the code needs to register to microsoft Azure to get encryption keys, and then can access the service for free for 30 days. After this time it can stil be used for free for small amount of characters to convert to audio. The python library to use this service is azure-cognitiveservices-speech. More information how to get set-up [here](https://docs.microsoft.com/en-us/azure/cognitive-services/speech-service/get-started-text-to-speech?tabs=script%2Cwindowsinstall&pivots=programming-language-python)




